### WallApp API

https://wallapp-api.herokuapp.com/

#### Requirements
- Django 1.10
- Django Rest Framework 3.5.3

#### Endpoints
- **GET /notes/** returns all notes (paginated by 10)
- **POST /notes/** creates a new note
- **PUT /notes/{id}/** updates an existing note
- **DELETE /notes/{id}/** deletes a note
- **POST /users/api-token-auth/** returns an auth token
- **GET /users/me/** returns the user data of the authenticated user
- **POST /users/** registers a new user and sends a confirmation mail

#### Setup
- `git clone https://gitlab.com/dapo/wallapp_api.git`
- `cd wallapp_api`
- `mkvirtualenv -p python3.5 wallapp_api`
- `pip install -r requirements.txt`
- `python manage.py migrate`
- `python manage.py runserver`

#### Tests
- `python manage.py test`