from django.contrib.sites.models import Site
from django.db import models
from rest_framework.authtoken.models import Token


class Note(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    text = models.TextField()
    user = models.ForeignKey('auth.User', related_name='notes',
                             on_delete=models.CASCADE)

    class Meta:
        ordering = ('-updated_at',)
