from rest_framework import serializers

from .models import Note


class NoteSerializer(serializers.ModelSerializer):
    username = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = Note
        fields = ('url', 'id', 'created_at', 'updated_at', 'text', 'username')
