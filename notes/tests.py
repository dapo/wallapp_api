import json

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class NoteTests(APITestCase):

    def setUp(self):
        super(NoteTests, self).setUp()
        self.password = 'mypassword'
        self.admin = User.objects.create_superuser('test', 'test@test.com',
                                                   self.password)

    def test_create_note_without_token(self):
        """
        Ensure we can not create a new note object without token.
        """
        url = reverse('note-list')
        data = {'text': 'Note text'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_note(self):
        """
        Ensure we can create a new note object.
        """
        self.client.login(username=self.admin.username, password=self.password)
        url = reverse('note-list')
        data = {'text': 'Note text'}
        response = self.client.post(url, data, format='json')
        note = json.loads(response.content.decode())
        self.assertEqual(note['username'], 'test')
        self.assertEqual(note['text'], 'Note text')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_note_exists(self):
        """
        Ensure that a created note object exists.
        """
        self.client.login(username=self.admin.username, password=self.password)
        url = reverse('note-list')
        data = {'text': 'Note text'}
        response = self.client.post(url, data, format='json')

        id = json.loads(response.content.decode())['id']
        url = reverse('note-detail', kwargs={'pk': id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        note = json.loads(response.content.decode())
        self.assertEqual(note['text'], 'Note text')

    def test_delete_note(self):
        """
        Ensure we can delete note objects.
        """
        self.client.login(username=self.admin.username, password=self.password)
        url = reverse('note-list')
        data = {'text': 'Note text'}
        response = self.client.post(url, data, format='json')

        id = json.loads(response.content.decode())['id']
        url = reverse('note-detail', kwargs={'pk': id})
        response = self.client.get(url)

        self.client.delete(json.loads(response.content.decode())['url'])
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_edit_note(self):
        """
        Ensure we can edit note objects.
        """
        self.client.login(username=self.admin.username, password=self.password)
        url = reverse('note-list')
        data = {'text': 'Note'}
        response = self.client.post(url, data, format='json')

        id = json.loads(response.content.decode())['id']
        url = reverse('note-detail', kwargs={'pk': id})
        response = self.client.get(url)
        note = json.loads(response.content.decode())
        self.assertEqual(note['text'], 'Note')

        data = {'text': 'Note text'}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(url)
        note = json.loads(response.content.decode())
        self.assertEqual(note['text'], 'Note text')
