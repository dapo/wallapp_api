from django.conf import settings
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def send_confirmation_mail(sender, instance=None, created=False, **kwargs):
    if created:
        from django.contrib.sites.models import Site
        from rest_framework.authtoken.models import Token
        from .models import Profile

        Token.objects.create(user=instance)
        Profile.objects.create(user=instance)

        # send confirmation email
        site = Site.objects.get_current().domain
        url = 'http://{site}{path}'.format(
            site=site,
            path=reverse(
                "user-confirm", kwargs={'uuid': str(instance.profile.id)}
            )
        )
        template_email_text = \
            'Please click on %s to confirm your account.' % url
        send_mail('WallApp Account',
                  template_email_text, 'michael.dauner@gmail.com',
                  [instance.email], fail_silently=False
                  )
