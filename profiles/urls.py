from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^$', views.CreateUserView.as_view(), name='user-create'),
    url(r'^me/$', views.me, name='user-me'),
    url(r'^confirm/(?P<uuid>[0-9a-z-]+)/', views.confirm, name='user-confirm'),

    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')
        ),
    url(r'^api-token-auth/', views.ObtainAuthTokenIfConfirmed.as_view()),
]
