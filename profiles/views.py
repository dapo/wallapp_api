from django.contrib.auth import get_user_model
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Profile
from .serializers import UserSerializer


class CreateUserView(generics.CreateAPIView):
    """
    This view allows to register a new user.
    """
    serializer_class = UserSerializer


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def me(request):
    """This view returns the data of the authenticated user"""
    User = get_user_model()
    user = get_object_or_404(User, pk=request.user.id)
    serializer = UserSerializer(user, context={'request': request})
    return Response(serializer.data)


@api_view(['GET'])
def confirm(request, **kwargs):
    """This view activates the user account."""
    try:
        profile = Profile.objects.get(pk=kwargs['uuid'])
    except Profile.DoesNotExist:
        return HttpResponse("Profile does not exist.")
    if not profile.confirmed:
        profile.confirmed = True
        profile.save()

    return HttpResponse("Your account is now activated.")


class ObtainAuthTokenIfConfirmed(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        if user.profile.confirmed:
            token, created = Token.objects.get_or_create(user=user)
            return Response({'token': token.key})
        # do not send token if account is not activated
        return Response({'error': 'account not activated'})
